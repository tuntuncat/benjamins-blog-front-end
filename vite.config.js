import vue from '@vitejs/plugin-vue'
import ElementPlus from 'unplugin-element-plus/vite'

// https://vitejs.dev/config/
export default {
  base: '/',
  plugins: [
    vue(),
    ElementPlus({
      // options
    })
  ],
  server: {
    port: 3002,
    host: true
  }
}
