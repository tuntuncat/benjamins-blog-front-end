// initial state

import axios from 'axios';

const state = () => ({
  blogsTotal: 0,
  blogsList: new Array(3).fill({}),
  categoriesCount: 0,
  categories: [],
});

// getters

// const getters = {
//   blogsTotal: state => state.blogsTotal,
//   currentPage: state => state.currentPage,
// }

// mutations

const mutations = {
  updateBlogTotal(state, payload) {
    state.blogsTotal = payload;
  },
  updateBlogsByPage(state, payload) {
    state.blogsList = payload;
  },
  updateCategoriesCount(state, payload) {
    state.categoriesCount = payload;
  },
  updateCategories(state, payload) {
    state.categories = payload;
  },
};

// actions

const actions = {
  async getBlogsTotal({ commit }) {
    const { data } = await axios.get('blogsTotal', {
      currentPage: state.currentPage,
    });
    commit('updateBlogTotal', data.total);
  },
  async handleCurrentPageChange({ commit }, payload) {
    const { data } = await axios.post('blogsByPageNum', {
      currentPage: payload,
    });
    commit('updateBlogsByPage', data);
  },
  async getCategoriesCount({ commit }, payload) {
    const { data } = await axios.get('categoriesCount');
    commit('updateCategoriesCount', data.count);
  },
  async getCategories({ commit }, payload) {
    const { data } = await axios.get('categories');
    commit('updateCategories', data.categories);
  },

};

export default {
  namespaced: true,
  state,
  // getters,
  actions,
  mutations,
};
