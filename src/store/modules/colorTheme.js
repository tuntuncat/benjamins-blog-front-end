const state = () => ({
  isDark: new Date().getHours() >= 18 || new Date().getHours() <= 9
})

const mutations = {
  changeColorTheme (state) {
    state.isDark =  !state.isDark
  }
}

export default {
  namespaced: true,
  state,
  mutations
}
