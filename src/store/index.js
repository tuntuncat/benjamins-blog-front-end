import { createStore } from "vuex";
import blogs from "./modules/blogs";
import colorTheme from "./modules/colorTheme";

export default createStore({
  modules: {
    blogs,
    colorTheme
  }
})
