import { createApp } from 'vue';
import App from './App.vue';
import router from './router/index';
import axios from 'axios';
import 'normalize.css';
import 'animate.css';
import 'default-passive-events/dist/index'
import '../public/darkMode.scss'
import store from './store';

const app = createApp(App);
axios.defaults.baseURL = 'http://120.24.212.148:3001/'
// axios.defaults.baseURL = 'http://127.0.0.1:3001/';

app.use(router);
app.use(store);
app.mount('#app');
