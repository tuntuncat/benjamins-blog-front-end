import {createRouter, createWebHistory} from 'vue-router';
import BlogItems from '../components/BlogItems.vue';
import Blog from '../components/BLog.vue';
import BlogList from '../components/BlogList.vue';
import Diaries from '../components/Diaries.vue';
import Playground from '../components/PlayGround.vue';
import SuperColorPicker from '../components/ColorPicker/SuperColorPicker.vue'
const routes = [
  {
    path: '/',
    component: BlogItems,
  },
  {
    path: '/blog/:id',
    component: Blog,
  },
  {
    path: '/blogs/:category',
    component: BlogList,
  },
  {
    path: '/blogs/all',
    component: BlogList,
  },
  {
    path: '/diaries',
    component: Diaries,
  },
  {
    path: '/playground',
    component: Playground,
  },
  {
    path: '/colorPicker',
    component: SuperColorPicker
  }
];

const router = createRouter({
  history: createWebHistory(),
  scrollBehavior (to, from, savedPosition) {
    if(to.hash){
      return {
        el: to.hash,
        top: 50,
        behavior: 'smooth'
      }
    }
  },
  routes,
});

export default router;
