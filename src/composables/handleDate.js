export default {
  transferTimeToPassedTime (baseTimeStamp, timeStamp) {
    const temp = Math.floor((baseTimeStamp - timeStamp) / 86400000)
    return temp === 0 ? `今天` : temp + '天前'
  },
  dayPassed (timeStamp) {
    return this.transferTimeToPassedTime(Date.now(), timeStamp)
  }
}
