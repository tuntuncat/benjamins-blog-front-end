import {computed} from "vue";
import {useStore} from 'vuex'


export default () => {
  const store = useStore()
  const isDark = computed(() => store.state.colorTheme.isDark)
  const modeChange = () => {
    store.commit('colorTheme/changeColorTheme')
  }
  return {
    isDark,
    modeChange
  }
}
